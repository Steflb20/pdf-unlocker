package at.flo.ui;

import at.flo.bl.DocLocker;
import at.flo.gui.LockerGUI;
import org.apache.pdfbox.pdmodel.encryption.InvalidPasswordException;

import java.io.File;
import java.io.IOException;

public class LockerUI {
    public static void main(String[] args) {
        boolean gui = false;
        String path = null;
        String mode = null;
        String pass = "";

        try {
            for (int i = 0; i < args.length; i++) {
                String arg = args[i];

                if (arg.equals("-g") || arg.equals("-gui")) {
                    gui = true;
                } else if (arg.equals("-lock") || arg.equals("-l")) {
                    mode = "lock";
                } else if (arg.equals("-unlock") || arg.equals("-u")) {
                    mode = "unlock";
                } else if (arg.equals("-f") || arg.equals("-file")) {
                    path = args[i + 1];
                } else if (arg.equals("-p") || arg.equals("-pass")) {
                    pass = args[i + 1];
                }
            }
        } catch (ArrayIndexOutOfBoundsException aioobe) {
            System.out.println("Not enough arguments found!");
        }

        File file = path == null ? null : new File(path);

        if (file != null) {
            if (file.exists()) {
                if (gui) {
                    new LockerGUI("PDF File (Un)locker", file).setVisible(true);
                } else {
                    if (mode != null) {
                        try {
                            if (mode.equals("lock")) {
                                DocLocker.lock(file, pass);
                            } else {
                                DocLocker.unlock(file, pass);
                            }
                        } catch (InvalidPasswordException ipe) {
                            System.out.println("Invalid password!");
                        } catch (IOException ioex) {
                            System.out.println("File not accessable!");
                        }
                    }
                }
            } else {
                System.out.println("This file doesnt exist!");
            }
        } else {
            if (gui) {
                new LockerGUI("PDF File (Un)locker", null).setVisible(true);
            } else {
                System.out.println("Please specify the file using \"-f\"");
            }
        }

    }
}
