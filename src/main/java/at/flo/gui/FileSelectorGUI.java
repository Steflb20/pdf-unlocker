package at.flo.gui;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.io.File;

public class FileSelectorGUI extends JFrame{
    private JPanel pnContent;
    private JFileChooser fcFileChooser;

    public FileSelectorGUI(String title) {
        setTitle(title);
        setContentPane(pnContent);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        initComponents();
        pack();
        setLocationRelativeTo(null);
    }

    private void initComponents() {
        fcFileChooser.removeChoosableFileFilter(fcFileChooser.getAcceptAllFileFilter());

        fcFileChooser.addChoosableFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                String name = f.getName();
                return name.substring(name.lastIndexOf('.') + 1).equals("pdf");
            }

            @Override
            public String getDescription() {
                return "[.pdf]";
            }
        });

        fcFileChooser.addActionListener(e -> {
            new LockerGUI("PDF File (Un)locker", fcFileChooser.getSelectedFile()).setVisible(true);
            this.dispose();
        });
    }

}
