package at.flo.gui;

import at.flo.bl.DocLocker;
import org.apache.pdfbox.pdmodel.encryption.InvalidPasswordException;

import javax.swing.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class LockerGUI extends JFrame{
    private JPanel pnContent;
    private JTextField tfFilePath;
    private JButton btSelectFile;
    private JRadioButton rbUnlock;
    private JRadioButton rbLock;
    private JButton btExecute;
    private JPasswordField pfPassword;

    private File file;

    public LockerGUI(String title, File file) {
        this.file = file;

        setTitle(title);
        setContentPane(pnContent);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        initComponents();
        pack();
        setLocationRelativeTo(null);
    }

    private void initComponents() {
        if (this.file == null) {
            this.tfFilePath.setText("No File Selected");
        } else {
            this.tfFilePath.setText(this.file.getAbsolutePath());
        }

        rbUnlock.addChangeListener(e -> {
            if (e.getSource() instanceof JRadioButton src) {
                if (src.isSelected()) {
                    btExecute.setText("Unlock File");
                } else {
                    btExecute.setText("Lock File");
                }
            }
        });

        this.btExecute.addActionListener(e -> {
            try {
                onExecute();
            } catch (InvalidPasswordException pwe) {
                JOptionPane.showMessageDialog(
                        null,
                        "The password was incorrect!",
                        "Invalid Password!",
                        JOptionPane.ERROR_MESSAGE
                );
            } catch (FileNotFoundException fnfe) {
                JOptionPane.showMessageDialog(
                        null,
                        "The chosen file wasn't found!",
                        "File Not Found!",
                        JOptionPane.ERROR_MESSAGE
                );
            } catch (IOException ioex) {
                JOptionPane.showMessageDialog(
                        null,
                        "Couldn't access file!",
                        "Access Error",
                        JOptionPane.ERROR_MESSAGE
                );
            }
        });

        btSelectFile.addActionListener(e -> onSelectFile());
    }

    private void onSelectFile() {
        new FileSelectorGUI("Choose a file").setVisible(true);
        this.dispose();
    }

    private void onLockFile() throws IOException {
        DocLocker.lock(this.file, String.copyValueOf(this.pfPassword.getPassword()));

        JOptionPane.showMessageDialog(
                null,
                "Locked File Successfully",
                "Success",
                JOptionPane.PLAIN_MESSAGE
        );
    }

    private void onUnlockFile() throws InvalidPasswordException, IOException {
        DocLocker.unlock(this.file, String.copyValueOf(this.pfPassword.getPassword()));

        JOptionPane.showMessageDialog(
                null,
                "Unlocked File Successfully",
                "Success",
                JOptionPane.PLAIN_MESSAGE
        );
    }

    private void onExecute() throws InvalidPasswordException, FileNotFoundException, IOException{
        if (this.file == null) {
            throw new FileNotFoundException();
        }

        if (rbLock.isSelected()) {
            onLockFile();
        } else {
            onUnlockFile();
        }

    }

}
