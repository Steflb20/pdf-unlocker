package at.flo.bl;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.encryption.AccessPermission;
import org.apache.pdfbox.pdmodel.encryption.StandardProtectionPolicy;

import java.io.File;
import java.io.IOException;

public class DocLocker {
    public static void unlock(File file, String pw) throws IOException {
        PDDocument doc = PDDocument.load(file, pw);
        doc.setAllSecurityToBeRemoved(true);
        doc.save(file);
        doc.close();
    }

    public static void lock(File file, String pw) throws IOException {
        PDDocument doc = PDDocument.load(file);
        AccessPermission perms = new AccessPermission();

        StandardProtectionPolicy stpp = new StandardProtectionPolicy(pw, pw, perms);
        stpp.setEncryptionKeyLength(256);

        doc.protect(stpp);
        doc.save(file);
        doc.close();
    }
}
