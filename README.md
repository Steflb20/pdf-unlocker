# PDF (Un)locker
Using this tool you can easily lock and unlock PDF-Files. It supports CLI and GUI usage for any platform. 

## About
The app was written in Java 17 and requires an up to date JRE (Java Runtime Environment) to be executed.

## Usage
| Flag                  | Function                                   |
|-----------------------|--------------------------------------------|
| -g / -gui             | launches the application into the GUI mode |
| -f / -file *path*     | specifies the path of the .pdf file        |
| -l / -lock            | specifies the program's mode to "lock"     |
| -u / -unlock          | specifies the program's mode to "unlock"   |
| -p / -pass *password* | specifies the password to lock/unlock      |

## Examples
Lock a file with the password "tomato"
```
java -jar pdfunlocker.jar -lock -file demo.pdf -pass tomato
```

Unlock this file
```
java -jar pdfunlocker.jar -unlock -file demo.pdf -pass tomato
```

Open the tool in GUI Mode:
```
java -jar pdfunlocker.jar -gui
```
